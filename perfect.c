#include<stdio.h>
int main()
{
        //declaration
        int n, sum = 0, i ;
        scanf("%d", &n);
       //printf(" line 1:  %d\n",n);
        //procedure
        for( i = 1 ; i < n ;i++)
        {
           // printf(" line %d\n",i);
            if((n % i )== 0)
                sum = sum + i ;
        }
        if( sum == n )
        {
            //    printf(" line s \n");
                printf("Perfect number");
        }
        else
                printf("not perfect number");
           //     printf(" line last");
        return 0;
}
/* n: n
output: perfect or not perfect number

procedure:
 n -> find factors of n
     n % i == 0 -> factor of n
 add them

 compare sum with the number.

factors of n: 1,2,3,6
sum of factors: 6
*/
